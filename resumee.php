<html>
    <head> 
        
        <img src = "Guia.png" height = "200" width = "200">
        
        <style>
            h1{
                font-family: 'Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;
            }
        </style> 

        <p>
        <h1> Guia Anne C. Cubelo</h1>
        </p>

        <link rel="stylesheet" href="resumeee.php">
    </head>

    <body>

            <style>
                h2{
                    font-family: 'Courier New', Courier, monospace; 
                }
            </style>
            <?php
            echo "<h2>CONTACT DETAILS</h2>";
             
            echo "<p><b>Address:  </b>P1, B5, L8 San Antonio St. Laverna Hills, Buhangin District, Davao City</p>";
            echo "<p><b>Mobile number: </b>+639053201517</p>";
            echo "<p><b>Email: </b>guia.cubelo@gmail.com</p>";
        
            echo "<h2>PERSONAL INFORMATION</h2>";
            echo "<p><b>Age: </b>20</p>";
            echo "<p><b>Date of Birth: </b>November. 28, 1997</p>";
            echo "<p><b>Gender: </b>Female</p>";
            echo "<p><b>Civil Status: </b>Single</p>";

            echo "<h2>EDUCATIONAL BACKGROUND </h2>";
            ?>

            
            <table border>
                    <tr>
                        <?php
                        echo "<td><b>LEVEL </b></td>";
                        echo "<td><b>SCHOOL</b></td>";
                        echo "<td><b>YEAR</b></td>";
                        ?>
                    </tr>

                    <tr>
                        <?php
                        echo "<td><b>Tertiary</b></td>";
                        echo "<td>University of Southeastern Philippines
                            (Bachelor of Science in Information Technology)
                            </td>";
                        echo "<td>2016 - present</td>";
                        ?>
                    </tr>

                    <tr>
                        <?php
                        echo "<td><b>Secondary</b></td>";
                        echo "<td>St Andrews College</td>";
                        echo "<td>2012 - 2015</td>";
                        ?>
                    </tr>

                    <tr>
                        <?php
                        echo "<td><b>Primary</b></td>";
                        echo "<td>The Kiddie Math Science Grade School</td>";
                        echo "<td>2004 -2011</td>";
                        ?>
                    </tr>
            </table>

            <?php
            echo "<h2>WORK EXPERIENCE</h2>";
            ?>

            <table border>
                    <tr>
                        <?php
                        echo "<td><b>YEAR</b></td>";
                        echo "<td><b>WORK</b></td>";
                        echo "<td><b>POSITION</b></td>";
                        ?>
                    </tr>

                    <tr>
                        <?php
                        echo "<td>2016 - 2017</td>";
                        echo "<td>Home-Based General Transcriptionist</td>";
                        echo "<td>Transcriptionist</td>";
                        ?>
                    </tr>

                    <tr>
                        <?php
                        echo "<td>2013 - 2015</td>";
                        echo "<td>Student Volunteer at St Andrews College</td>";
                        echo "<td>Examination Reader and Writer</td>";
                        ?>
                    </tr>
            </table>

            <?php
            echo "<h2>TECHNICAL SKILLS</h2>"; 
            echo "<p>Basic Computer Skills</p>";
            echo "<p>Adobe Creative Photoshop</p>";

            echo "<h2>PERSONAL SKILLS</h2>";
            echo "<p>Ability to work independently or as part of the team</p>";
            echo "<p>Highly organized</p>";
            echo "<p>Housekeeping</p>";
            echo "<p>Self-motivation and decision making</p>";

            echo "<h2>REFERENCES</h2>";
            echo "<p>Donna Belle C. Limpag, RN, Freelance Home-Based Transcriptionist, 09234786829 / 285-2723</p>";
            echo "<p>Ms. Karen Smith, Leader of Learning Support, 02 9626 4000, 02 9626 4099</p>";
            ?>
            
        </body>
</html>